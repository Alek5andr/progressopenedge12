@openapi.openedge.export FILE(type="REST", executionMode="singleton", useReturnValue="false", writeDataSetBeforeImage="false").
 
 /*------------------------------------------------------------------------
    File        : Client
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : User
    Created     : Sun Feb 28 23:16:54 EET 2021
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS Client: 
    DEFINE TEMP-TABLE addClientTT LIKE client.
    DEFINE TEMP-TABLE getClientTT LIKE client.
    DEFINE TEMP-TABLE getClientsTT LIKE client.
    DEFINE TEMP-TABLE updateClientTT LIKE client.
    
    DEFINE DATASET clientSet FOR updateClientTT.
    
        /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID createClient( INPUT firstName AS CHARACTER, INPUT lastName AS CHARACTER, INPUT phoneNumber AS CHARACTER, OUTPUT successMsg AS CHARACTER, OUTPUT isSuccess AS LOGICAL ):
        
        CREATE addClientTT.
        
        ASSIGN
            addClientTT.firstName = firstName
            addClientTT.lastName = lastName
            addClientTT.phoneNumber = phoneNumber
            addClientTT.clientId = getNewClientId().
            
        BUFFER-COPY addClientTT TO client.
        
        successMsg = "Client is added".
        isSuccess = TRUE.           

    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getClient( INPUT clientId AS INTEGER, OUTPUT TABLE getClientTT, OUTPUT successMsg AS CHARACTER, OUTPUT isSuccess AS LOGICAL ):
        EMPTY TEMP-TABLE getClientTT.
        
        FIND FIRST client WHERE client.clientId EQ clientId NO-LOCK NO-ERROR.
        
        IF AVAILABLE client THEN
        DO:
            CREATE getClientTT.
            BUFFER-COPY client TO getClientTT.
            
            successMsg = "Client with ID is fetched successfully: " + STRING(clientId).
            isSuccess = TRUE.
        END.
        ELSE
        DO:
            successMsg = "Client with ID is not fetched: " + STRING(clientId).
            isSuccess = FALSE.
        END.

    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getClients( OUTPUT TABLE getClientsTT, OUTPUT successMsg AS CHARACTER, OUTPUT isSuccess AS LOGICAL ):
        
        EMPTY TEMP-TABLE getClientsTT.
        
        FOR EACH client NO-LOCK:
            CREATE getClientsTT.
            BUFFER-COPY client TO getClientsTT.
        END.
        
        successMsg = "Clients are fetched successfully".
        isSuccess = TRUE.

    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID updateClient( INPUT clientId AS INTEGER, DATASET clientSet, OUTPUT TABLE updateClientTT, OUTPUT successMsg AS CHARACTER, OUTPUT isSuccess AS LOGICAL ):
        
        FIND FIRST updateClientTT.
        
        FIND FIRST client WHERE client.clientId EQ clientId EXCLUSIVE-LOCK NO-ERROR.
        
        ASSIGN
            updateClientTT.clientId = clientId.
            
        IF AVAILABLE client THEN
        DO:
            CREATE updateClientTT.
            BUFFER-COPY updateClientTT TO client.
            successMsg = "Client with ID is updated successfully: " + STRING(clientId).
            isSuccess = TRUE.
        END.
        ELSE
        DO:
            successMsg = "Client with ID is not updated: " + STRING(clientId).
            isSuccess = FALSE.
        END.

    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID deleteClient( INPUT clientId AS INTEGER, OUTPUT successMsg AS CHARACTER, OUTPUT isSuccess AS LOGICAL ):
        
        FIND FIRST client WHERE client.clientId EQ clientId NO-LOCK NO-ERROR.
        
        IF AVAILABLE client THEN
        DO:
            DELETE client.
            successMsg = "Client with ID is deleted successfully: " + STRING(clientId).
            isSuccess = FALSE.
        END. 
        ELSE
        DO:
            successMsg = "Client with ID is not deleted: " + STRING(clientId).
            isSuccess = FALSE.
        END.
        
    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose:
     Notes: Candidate for separate reusable class (procedure?) => Book.getNewBooktId
    ------------------------------------------------------------------------------*/

    METHOD PRIVATE INTEGER getNewClientId(  ):
        
        DEFINE VARIABLE result AS INTEGER NO-UNDO.
        
        FIND LAST client NO-LOCK NO-ERROR.
        
        IF AVAILABLE client THEN
        DO:
            RESULT = client.clientId.
        END.
        
        REPEAT:
            FIND FIRST client WHERE client.clientId EQ RESULT + 1 NO-LOCK NO-ERROR.
            RESULT = client.clientId + 1.
            IF NOT AVAILABLE client THEN LEAVE.
        END.

        RETURN result.

    END METHOD.

END CLASS.