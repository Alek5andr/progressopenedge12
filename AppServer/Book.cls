@openapi.openedge.export FILE(type="REST", executionMode="singleton", useReturnValue="false", writeDataSetBeforeImage="false").
 
 /*------------------------------------------------------------------------
    File        : Book
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : User
    Created     : Sun Feb 28 14:45:02 EET 2021
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS Book: 
    DEFINE TEMP-TABLE getBooksTT LIKE book.
    DEFINE TEMP-TABLE getBookTT LIKE book.
    DEFINE TEMP-TABLE addBookTT LIKE book. 
    DEFINE TEMP-TABLE updateBookTT LIKE book.
    
    DEFINE DATASET bookSet FOR updateBookTT.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID createBook( INPUT bookTitle AS CHARACTER, bookAuthor AS CHARACTER, bookYear AS INTEGER, OUTPUT successMsg AS CHARACTER, OUTPUT isSuccess AS LOGICAL ):
        CREATE addBookTT.
        
        ASSIGN
            addBookTT.bookTitle = bookTitle
            addBookTT.bookAuthor = bookAuthor
            addBookTT.bookYear = bookYear
            addBookTT.bookId = getNewBookId().
        
        BUFFER-COPY addBookTT TO book.
    
        successMsg = "Book is added".
        isSuccess = TRUE.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getBook( INPUT bookId AS INTEGER, OUTPUT TABLE getBookTT, OUTPUT successMsg AS CHARACTER, OUTPUT isSuccess AS LOGICAL ):
        EMPTY TEMP-TABLE getBookTT.
        
        FIND FIRST book WHERE book.bookId EQ bookId NO-LOCK NO-ERROR.
        /*DEFINE VARIABLE bookEntity AS book.
        bookEntity = getBookEntity(bookId).*/
        
        //IF AVAILABLE bookEntity THEN
        IF AVAILABLE book THEN
        DO:
            CREATE getBookTT.
            BUFFER-COPY book TO getBookTT.
        
            successMsg = "Book is fetched by ID successfully: " + STRING(bookId).
            isSuccess = TRUE.
        END.
        ELSE
        DO:
            successMsg = "Book is not fetched by ID: " + STRING(bookId).
            isSuccess = FALSE.
        END.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getBooks( OUTPUT TABLE getBooksTT, OUTPUT successMsg AS CHARACTER, OUTPUT isSuccess AS LOGICAL ):
        EMPTY TEMP-TABLE getBooksTT.
        
        FOR EACH book NO-LOCK:
            CREATE getBooksTT.
            BUFFER-COPY book TO getBooksTT.
        END.
        
        successMsg = "Books are fetched successfully".
        isSuccess = TRUE.
    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID updateBook( INPUT bookId AS INTEGER, DATASET bookSet, OUTPUT TABLE updateBookTT, OUTPUT successMsg AS CHARACTER, OUTPUT isSuccess AS LOGICAL ):
        FIND FIRST updateBookTT.
        
        FIND FIRST book WHERE book.bookId EQ bookId EXCLUSIVE-LOCK NO-ERROR.
        
        ASSIGN
            updateBookTT.bookId = bookId.
            
        IF AVAILABLE book THEN
        DO:
            CREATE updateBookTT.
            BUFFER-COPY updateBookTT TO book.
            successMsg = "Book with ID is updated successfully: " + STRING(bookId).
            isSuccess = TRUE.
        END.
        ELSE
        DO:
            successMsg = "Book with ID is not updated: " + STRING(bookId).
            isSuccess = FALSE.
        END.
    END METHOD.
    
     /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID deleteBook( INPUT bookId AS INTEGER, OUTPUT successMsg AS CHARACTER, OUTPUT isSuccess AS LOGICAL ):
        FIND FIRST book WHERE book.bookId EQ bookId EXCLUSIVE-LOCK NO-ERROR.
        
        IF AVAILABLE book THEN
        DO:
            DELETE book.
            successMsg = "Book with ID is deleted successfully: " + STRING(bookId).
            isSuccess = TRUE.
        END.
        ELSE
        DO:
            successMsg = "Book with ID is not deleted: " + STRING(bookId).
            isSuccess = TRUE.
        END.
        
    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose:
     Notes: Candidate for separate reusable class (procedure?)
    ------------------------------------------------------------------------------*/

    METHOD PRIVATE INTEGER getNewBookId(  ):
        DEFINE VARIABLE bookId AS INTEGER INIT 0.
        
        FIND LAST book NO-LOCK NO-ERROR.
        
        IF AVAILABLE book THEN
        DO:
            bookId = book.bookId.
        END.
        
        REPEAT:
            FIND FIRST book WHERE book.bookId = bookId + 1 NO-LOCK NO-ERROR.
            bookId = book.bookId + 1.
            IF NOT AVAILABLE book THEN LEAVE.
        END.
        
        RETURN bookId.
    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose: Simplify maintainability for reusable search logic of book entity
     Notes: Could not figure out why returned data entity is ambiguous in Book.getBook()
     When figured out, then can be optimized for Client entity search
    ------------------------------------------------------------------------------*/
    
    METHOD PRIVATE book getBookEntity( INPUT bookId AS INTEGER ):      
        DEFINE VARIABLE error AS Progress.Lang.AppError NO-UNDO.
        error = NEW Progress.Lang.AppError("No book record is found", 200).
        DEFINE VARIABLE bookEntity AS book.
        
        FIND FIRST book WHERE book.bookId EQ bookId EXCLUSIVE-LOCK NO-ERROR.
        IF AVAILABLE book THEN
        DO:
            RETURN bookEntity.
        END.
    END METHOD.
    
END CLASS.